#!/usr/bin/env python
from __future__ import print_function
import hashlib, os, subprocess, sys

if len(sys.argv) < 3:
    print('Usage: {} dir1 dir2'.format(sys.argv[0]))
    sys.exit(1)

dir1 = os.path.realpath(sys.argv[1])
dir2 = os.path.realpath(sys.argv[2])

for root, dirs, files in os.walk(dir1):
    for f in files:
        f1 = '{}/{}'.format(root, f)
        f2 = f1.replace(dir1, dir2, 1)

        if(os.path.isfile(f2)):

            # Don't diff files over 1MiB
            if os.stat(f1).st_size > 1048576 or os.stat(f2).st_size > 1048576: continue

            # Check if files are the same; in which case a diff is useless
            h1 = hashlib.sha256(open(f1, 'rb').read()).hexdigest()
            h2 = hashlib.sha256(open(f2, 'rb').read()).hexdigest()
            if h1 == h2: continue

            # Don't diff binary files
            if open(f1, 'rb').read().find(b'\000') >= 0: continue

            subprocess.call(['vimdiff', f1, f2])

